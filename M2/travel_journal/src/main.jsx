import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { Header } from "./components/Header";
import { Main } from "./components/Main";
import { Footer } from "./components/Footer";

ReactDOM.createRoot(root).render(
  <>
    <Header />
    <Main />
    <Footer />
  </>
);
