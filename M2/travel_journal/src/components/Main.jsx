import React from 'react';
import data from '../data.js';
import { Post } from './post/Post.jsx';

export const Main = () => {
  return (
    <main>
      {data.map((post, index, array) => {return (
          <Post
            key={post.id}
            {...post}
            isLastPost={index == array.length-1}
          />
      )})}
    </main>
  );
};
