import React from 'react';

export const Location = ({location, googleMapsUrl}) => {
  return (
    <div className='location'>
      <img className='location__mapPointer' src={`/img/map_pointer.png`}/>
      <span className='location__country'>{location.toUpperCase()}</span>
      <a className='location__googleMaps' href={googleMapsUrl}>View on Google Maps</a>
    </div>
  );
};
