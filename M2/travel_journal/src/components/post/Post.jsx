import React from 'react';
import { Location } from './Location.jsx';

export const Post = (props) => {
  return (
    <div className='post'>
      <div className='post__container'>
        <img className='post__img' src={`/img/${props.imageUrl}`} />
        <div className='post__content'>
          <Location
            location={props.location}
            googleMapsUrl={props.googleMapsUrl}
          />
          <h1 className='post__title'>{props.title}</h1>
          <h5 className='post__date'>{props.startDate} - {props.endDate}</h5>
          <p className='post__description'>{props.description}</p>
        </div>
      </div>
      {!props.isLastPost && <hr/>}
    </div>
  );
};
