import React from "react";

export const Header = () => {
  return (
    <header>
      <img className='header__logo' src='/img/logo.png'/>
      <h3 className='header__title'>{`Travel Journal`}</h3>
    </header>
  );
};
