import React from "react";
import { Header } from "./components/Header.jsx";
import { Meme } from "./components/Meme.jsx";

export const App = () => {
  return (
    <div>
      <Header />
      <Meme />
    </div>
  );
}
