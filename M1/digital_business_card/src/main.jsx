import React from 'react'
import ReactDOM from 'react-dom/client'
import {Header} from './components/Header.jsx';
import {Main} from './components/Main.jsx';
import {Footer} from './components/Footer.jsx';
import './index.css'

ReactDOM.createRoot(root).render(
  <div className='app'>
      <Header />
      <Main />
      <Footer />
  </div>
)
