import React from 'react';

export const Header = () => {
    return(
        <header>
            <img className="header__img" src="/img/eugene.jpg" />
        </header>
    );
};