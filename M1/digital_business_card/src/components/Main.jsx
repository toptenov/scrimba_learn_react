import React from "react";

export const Main = () => {
  return (
    <main>
      <h1 className="main__title">Eugene Pimenov</h1>
      <h3 className="main__subtitle">Frontend Developer</h3>
      <p className="main__text">@eugene.pimenov</p>
      <button
        className="main__email"
        type="submit"
        onClick={() => {
          window.open("https://gmail.com");
        }}
      >
        Email
      </button>
      <div className="main__projects">
        <h2>React projects</h2>
        <p>
          <a href="https://eugene-pimenov-notes.netlify.app/">Notes</a>{" "}
          - Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
          officia deserunt mollit anim id est laborum
        </p>
        <p>
          <a href="https://eugene-pimenov-tenzies.netlify.app/">
            Tenzies
          </a>{" "}
          - Cum sociis natoque penatibus et magnis dis parturient montes,
          nascetur ridiculus mus
        </p>
        <p>
          <a href="https://eugene-pimenov-travel-journal.netlify.app/">
            Meme Generator
          </a>{" "}
          - Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
          commodo ligula eget dolor.
        </p>
        <p>
          <a href="https://eugene-pimenov-meme-generator.netlify.app/">
            Travel Journal
          </a>{" "}
          - Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.
        </p>
      </div>
      <div className="main__about">
        <h2>About</h2>
        <p>
          Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
          Nulla consequat massa quis enim. Donec pede justo, fringilla vel,
          aliquet nec, vulputate eget, arcu
        </p>
      </div>
      <div className="main__interests">
        <h2>Interests</h2>
        <p>
          In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam
          dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.
          Vivamus elementum semper nisi
        </p>
      </div>
    </main>
  );
};
