import React from 'react';

export const Footer = () => {
    return(
        <footer>
                <a href="https://telegram.org"><img className="footer__telegram" src="/img/telegram.png"/></a>
                <a href="https://vk.com"><img className="footer__vk" src="/img/vk.png"/></a>
                <a href="https://youtube.com"><img className="footer__youtube" src="/img/youtube.png"/></a>
                <a href="https://github.com"><img className="footer__github" src="/img/github.png"/></a>
        </footer>
    );
};